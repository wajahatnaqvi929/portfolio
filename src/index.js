import React,{useState} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch  } from 'react-router-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import SignIn from './signIn';
import CreateAccount from './createAccount'
import Portfolio from './portfolio';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';
import { PrivateRoute } from "./privateRoute";






const root = ReactDOM.createRoot(document.getElementById('root'));


function AppContainer() {
  const [showCreateAccount, setShowCreateAccount] = useState(false);

  const handleCreateAccountClick = () => {
    setShowCreateAccount(true);


   
};
const handleSignInClick =()=>{
  setShowCreateAccount(false);

}


return(


  <>


  
    {/* <React.StrictMode>

  <App />
  </React.StrictMode>
  
  <div>
<p>HI this is from the index file</p>
  </div> */}



{/* <App/> */}
<BrowserRouter>
      <Switch>
        <Route exact path="/">
          <div className="d-flex w-100 ">
            <div className="signIn w-50  bg-dark d-flex justify-content-center align-items-center">
              {showCreateAccount ? (
                <CreateAccount onSignInClick={handleSignInClick} />
              ) : (
                <div className=" p-5">
                  <SignIn onCreateAccountClick={handleCreateAccountClick} />
                </div>
              )}
            </div>
            <div className="img content w-50">
              <div className="content"></div>
            </div>
          </div>
        </Route>
        <Route path="/portfolio" component={Portfolio} />
      </Switch>
    </BrowserRouter>


  </>
)
}

root.render(
  
  <AppContainer />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
