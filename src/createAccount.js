import React, {useState} from "react";
import "./createAccount.css";
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useSpring, animated } from 'react-spring';




function CreateAccount({onSignInClick}){


    const [showPassword, setShowPassword] = useState(false);
    const props = useSpring({ opacity: 1,
      transform: 'scale(1)',
      from: { opacity: 0, transform: 'scale(0)' }, });






    const handleShowPassword = () => {
      setShowPassword(!showPassword);
    };
  
    const [username, setUsername] = useState("");
    const [email, setemail] = useState("");
    const [confirmPass, setconfirmPass] = useState("");
    const [password, setPassword] = useState("");
    const [usernameError, setUsernameError] = useState("");
    const [emailError, setEmailError] = useState("");
    const [confirmPassError, setConfirmPassError] = useState("");
    const [passwordError, setPasswordError] = useState("");
  
    const handleSignIn = (event) => {
      event.preventDefault(); // prevent the form from submitting
  
      // validate the username
      if (username.trim() === "") {
        setUsernameError("Username is required");
      } else {
        setUsernameError("");
      }
      if (email.trim() === "") {
        setEmailError("Username is required");
      } else {
        setEmailError("");
      }
      if (confirmPass.trim() === "") {
        setConfirmPassError("Username is required");
      } else {
        setConfirmPassError("");
      }
  
      // validate the password
      if (password.trim() === "") {
        setPasswordError("Password is required");
      } else {
        setPasswordError("");
      }
  
      // submit the form if there are no errors
      if (usernameError === "" && passwordError === "" && confirmPassError==='' && emailError === '') {
        console.log("Submitting form...");
        // add your logic to submit the form
      }
    };


    
    const handleCreateAccount = async () => {
        try {
            const response = await fetch('http://localhost:3000/api/users', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              username,
              email,
              password,
            }),
          });
          const data = await response.json();
          console.log(data);
          // show success message or redirect to login page

          if(data.status==true){
            toast.success(data.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          if(data.status==false){
            toast.danger(data.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
        } catch (error) {
          console.log(error);
          // show error message
          
        }
      };
      

    
    return(

      <animated.div  style={props}>
      <ToastContainer />
      {/* your app content */}
    
        <div className="card p-5 ">
        <p className="text-light mt-4 text-uppercase">Create Account</p>
  
        <form onSubmit={handleSignIn}>
          <div className="inputbox w-100">
            <input
              required="required"
              type="text"
              value={username}
              onChange={(event) => setUsername(event.target.value)}
            />
            <span>Username</span>
            <i></i>
          </div>
          <div className="inputbox mt-4 w-100">
            <input
              required="required"
              type="text"
              value={email}
              onChange={(event) => setemail(event.target.value)}
            />
            <span>Email</span>
            <i></i>
          </div>
  
          <div className="d-flex mt-4 w-100 justify-content-center align-items-center">
            <div className="inputbox w-100">
              <input
                required="required"
                type={showPassword ? "text" : "password"}
                value={password}
                onChange={(event) => setPassword(event.target.value)}
              />
              <span>Password</span>
              <i></i>
            </div>
  
            <div>
              <i
                className={
                  showPassword
                    ? "fas fa-eye-slash text-light ms-2"
                    : "fas fa-eye text-light ms-2"
                }
                onClick={handleShowPassword}
              ></i>
            </div>
          </div>
          <div className="d-flex mt-4 w-100 justify-content-center align-items-center">
            <div className="inputbox w-100">
              <input
                required="required"
                type={showPassword ? "text" : "password"}
                value={password}
                onChange={(event) => setconfirmPass(event.target.value)}
              />
              <span>Password</span>
              <i></i>
            </div>
  
            <div>
              <i
                className={
                  showPassword
                    ? "fas fa-eye-slash text-light ms-2"
                    : "fas fa-eye text-light ms-2"
                }
                onClick={handleShowPassword}
              ></i>
            </div>
          </div>
  
          {/* <div className="text-danger">{usernameError}</div>
          <div className="text-danger">{passwordError}</div> */}
  
          <button className="button my-4 mb-3" onClick={handleCreateAccount}>Create Account</button>
        </form>
  
        <div>
          <span className="text-light ">
            Already Have Account?{" "}
            <span role="button" style={{ color: "#2c9caf" }} onClick={onSignInClick}>
             SignIn
            </span>
          </span>
        </div>
      </div>
      </animated.div>
    )
}

export default CreateAccount