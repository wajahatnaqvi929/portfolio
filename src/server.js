const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');

require('dotenv').config({ path: './jwt.env' }); // Load environment variables

const MONGODB_URI='mongodb+srv://wajahatnaqvi929:wajahatnaqvi737@cluster0.2r48phw.mongodb.net/portfolioDb'
mongoose.connect(MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverSelectionTimeoutMS: 5000 // set the timeout to 5 seconds
});

const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to MongoDB'));

const userSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
});

const User = mongoose.model('User', userSchema);

app.use(express.json());

app.post('/api/users', async (req, res) => {
  try {
    // Check if username and email already exist
    const { username, email, password } = req.body;
    const existingUser = await User.findOne({ $or: [{ username }, { email }] });
    if (existingUser) {
      return res.status(409).json({ message: 'Username or email already exists', status:false });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user in the database
    const newUser = new User({ username, email, password: hashedPassword });
    await newUser.save();
    console.log('Received POST request to /api/users');
    res.status(201).json({ message: 'User created successfully', status:true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

app.post('/api/login', async (req, res) => {
  try {
    // Check if username or email exists
    const { usernameOrEmail, password } = req.body;
    const user = await User.findOne({
      $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }],
    });
    if (!user) {
      // If user not found, return error response
      return res.status(401).json({ message: 'Invalid username or email',status:false });
    }

    // Validate password
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      // If password does not match, return error response
      return res.status(401).json({ message: 'Invalid password', status:false });
    }

    // If username/email and password are valid, create and return a token

  const token = jwt.sign({ id: user._id },'wajahatnaqvi737');
  res.status(200).json({ 
    message: 'User login successful',
    token,
    status:true
  });

  console.log('Received POST request to /api/login');
  // res.status(201).json({ message: 'User SignIN successfully' });


  } catch (error) {

    console.error(error);
  res.status(500).json({ message: 'Internal server error', error: error });
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server started on port ${port}`));
