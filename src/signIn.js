import "./signIn.css"; // import the CSS file
import React, { useState } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useSpring, animated } from 'react-spring';
import { useHistory } from "react-router-dom";

function SignIn({ onCreateAccountClick }) {
  const [showPassword, setShowPassword] = useState(false);
  const props = useSpring({ opacity: 1,
    transform: 'scale(1)',
    from: { opacity: 0, transform: 'scale(0)' }, });

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };


  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const history =useHistory();
  const handleSignIn = (event) => {
    event.preventDefault(); // prevent the form from submitting

    // validate the username
    if (username.trim() === "") {
      setUsernameError("Username is required");
    } else {
      setUsernameError("");
    }

    // validate the password
    if (password.trim() === "") {
      setPasswordError("Password is required");
    } else {
      setPasswordError("");
    }

    // submit the form if there are no errors
    if (usernameError === "" && passwordError === "") {
      console.log("Submitting form...");
      // add your logic to submit the form
    }
  };

//Api 



const handleSignInApi = async () => {
  try {
      const response = await fetch('http://localhost:3000/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        usernameOrEmail: username,
        password,
      }),
    });
    const data = await response.json();
    console.log(data);
    // show success message or redirect to login page
    
if(data.status===true){
  toast.success(data.message, {
    position: toast.POSITION.BOTTOM_RIGHT
  });
 
  history.push("/portfolio"); // redirect to portfolio page

}
if(data.status===false){
  toast.error(data.message, {
    position: toast.POSITION.BOTTOM_RIGHT
  });
}
   
  } catch (error) {
    console.log(error);
    // show error message
    
  }
};



// const Login = () => {
//   const history = useHistory();

//   const handleSignIn = async () => {
//     const data = await handleSignInApi(username, password);
//     if (data.status) {
//       toast.success(data.message, {
//         position: toast.POSITION.BOTTOM_RIGHT,
//       });
//       history.push('/portfolio');
//     } else {
//       toast.error(data.message, {
//         position: toast.POSITION.BOTTOM_RIGHT,
//       });
//     }
//   };

// };






  return (



    <animated.div style={props}>
    <ToastContainer />
    {/* your app content */}
    <div className="card p-5">
      <p className="text-light">SIGN IN</p>

      <form onSubmit={handleSignIn}>
        <div className="inputbox w-100">
          <input
            required="required"
            type="text"
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
          <span>Username</span>
          <i></i>
        </div>

        <div className="d-flex mt-4 w-100 justify-content-center align-items-center">
          <div className="inputbox w-100">
            <input
              required="required"
              type={showPassword ? "text" : "password"}
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            <span>Password</span>
            <i></i>
          </div>

          <div>
            <i
              className={
                showPassword
                  ? "fas fa-eye-slash text-light ms-2"
                  : "fas fa-eye text-light ms-2"
              }
              onClick={handleShowPassword}
            ></i>
          </div>
        </div>

        {/* <div className="text-danger">{usernameError}</div>
        <div className="text-danger">{passwordError}</div> */}

        <button className="button mt-5" onClick={handleSignInApi}>SIGN in</button>
      </form>

      <div className="pt-2" >
        <span className="text-light ">
          Don't Have Account?{" "}
          <span role="button" style={{ color: "#2c9caf" }} onClick={onCreateAccountClick}>
            Create One
          </span>
        </span>
      </div>
    </div>


    </animated.div>
  );

}
export default SignIn;
