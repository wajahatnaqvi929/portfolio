import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ path, component, isAuthenticated }) => {
  return (
    <Route
      path={path}
      render={(props) => {
        if (isAuthenticated) {
          return <component {...props} />;
        } else {
          return <Redirect to="/" />;
        }
      }}
    />
  );
};

export { PrivateRoute };
